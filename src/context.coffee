
class Context

  silent = false
  storage = {}

  log_error = (error) -> console.error(error) unless silent
  delay = (delay, cb) -> setTimeout(cb, delay)

  localStorage =
    channels:
      get: (id, clbck) ->
        clbck null, storage.channels[id]
      save: (obj, clbck) ->
        return clbck 'Id is not set' unless obj.id
        storage.channels[obj.id] = obj
        clbck null
      all: (clbck) ->
        clbck null, (channel for id, channel of storage.channels)
    teams:
      get: (id, clbck) ->
        clbck null, storage.teams[id]
      save: (obj, clbck) ->
        return clbck 'Id is not set' unless obj.id
        storage.teams[obj.id] = obj
        clbck null
      all: (clbck) ->
        clbck null, (team for id, team of storage.teams)
    users:
      get: (id, clbck) ->
        clbck null, storage.users[id]
      save: (obj, clbck) ->
        return clbck 'Id is not set' unless obj.id
        storage.users[obj.id] = obj
        clbck null
      all: (clbck) ->
        clbck null, (user for id, user of storage.users)


  constructor: (@controller, options={}) ->
    {@scope='channels', silent=false, @ttl=600000, @storageType='memory'} = options
    @scopeName = @scope.substr(0, @scope.length - 1)
    if @storageType is 'controller'
      @storage = @controller.storage
    else
      storage = {channels: {}, teams: {}, users: {}}
      @storage = localStorage
    @registerMiddleware()


  registerMiddleware:  ->
    @controller.middleware.receive.use (bot, message, next) =>
      @addContextToMessage(message)
        .then () ->
          next()
          return
        .catch (err) ->
          log_error(err)
          next()


  addContextToMessage: (message) ->
    @getFullContextStack(message)
      .then (stack) ->
        message.context_stack = stack
        message.context = stack[stack.length - 1] if stack.length


  pushContext: (message, context) ->
    new Promise (resolve, reject) =>
      return reject(new Error('No context given')) unless context
      return reject(new Error("Msg property #{@scopeName} is not set")) unless message[@scopeName]
      @storage[@scope].get message[@scopeName], (err, data) =>
        return reject(err) if err
        data = data or {id: message[@scopeName]}
        data.context_stack = data.context_stack or []
        context = {id: context} if typeof context is 'string'
        context.timestamp = +(new Date())
        data.context_stack.push context
        @storage[@scope].save data, (err) =>
          return reject(err) if err
          delay @ttl, () => @popContext message
          resolve(context)


  popContext: (message) ->
    new Promise (resolve, reject) =>
      @storage[@scope].get message[@scopeName], (err, data) =>
        return reject(err) if err
        if data and data.context_stack and data.context_stack.length
          poped_context = data.context_stack.pop()
          @storage[@scope].save data, (err) ->
            return reject(err) if err
            resolve poped_context
        else
          resolve()


  getContext: (message) ->
    new Promise (resolve, reject) =>
      @storage[@scope].get message[@scopeName], (err, data) ->
        return reject(err) if err
        if data and data.context_stack and data.context_stack.length
          resolve(data.context_stack[data.context_stack.length - 1])
        else
          resolve()


  updateContext: (message, ctx) ->
    new Promise (resolve, reject) =>
      @storage[@scope].get message[@scopeName], (err, data) =>
        return reject(err) if err
        data = data or {id: message[@scopeName]}
        data.context_stack = data.context_stack or []
        for context, index in data.context_stack
          if context.id is ctx.id
            ctx.timestamp = +(new Date())
            data.context_stack.splice(index, 1, ctx)
            @storage[@scope].save data, (err) ->
              return reject(err) if err
              resolve(ctx)
            return


  popContextsUntilContext: (message, contextId) ->
    new Promise (resolve, reject) =>
      return reject("Msg property #{@scopeName} not valid") unless message[@scopeName]
      @storage[@scope].get message[@scopeName], (err, data) =>
        return reject(err) if err
        if not data or not data.context_stack
          resolve()
          return
        stack = data.context_stack
        i = 0
        for ctx, i in stack
          if ctx.id is contextId
            break
        stack.splice(i)
        data.context_stack = stack
        @storage[@scope].save data, (err) =>
          return reject(err) if err
          delay @ttl, () => @popContext message
          resolve()


  getFullContextStack: (message) ->
    new Promise (resolve, reject) =>
      return reject("Msg property #{@scopeName} not valid") unless message[@scopeName]
      @storage[@scope].get message[@scopeName], (err, data) ->
        return reject(err) if err
        resolve(data and data.context_stack or [])


  expectedContexts: (targetContexts=[]) ->
    return (patterns, message) ->
      for context in message.context_stack
        if context.id in targetContexts
          for pattern in patterns
            pattern = new RegExp(pattern) if typeof(pattern) is 'string'
            if pattern.test(message.text)
              return true
      return false


  expectedTopContext: (targetContext) ->
    return (patterns, message) ->
      if message.context and message.context.id is targetContext
        for pattern in patterns
          pattern = new RegExp(pattern) if typeof(pattern) is 'string'
          if pattern.test(message.text)
            return true
        return false
      return false


  killAllContexts: (message=null) ->
    new Promise (resolve, reject) =>
      @storage[@scope].get message[@scopeName], (err, data) =>
        return reject(err) if err
        if data and data.context_stack
          data.context_stack = []
          @storage[@scope].save data, (err) ->
            return reject(err) if err
            resolve()
        else
          resolve()


module.exports = Context
