chai = require 'chai'
sinon = require 'sinon'
chaiAsPromised = require 'chai-as-promised'

should = chai.should()
chai.use(chaiAsPromised)
chai.use(require 'sinon-chai')

Context = require '../src/context'


describe 'Context', ->

  describe 'Storage', ->

    beforeEach ->
      @message = {channel: 'ID123', text: 'hey hey'}
      storage = {}
      @controller =
        storage:
          channels:
            get: (id, clbck) -> clbck(null, storage[id])
            save: (obj, clbck) ->
              storage[obj.id] = obj
              clbck(null)
        middleware:
          receive:
            use: (cb) => cb({}, @message, () -> {})
      @context = new Context(@controller, scope: 'channels')


    it 'should add context in stack', ->
      context = id: 'main_context'
      @context.pushContext @message, context
        .then () => @context.getContext(@message)
        .should.eventually.equal context


    it 'should update context in stack', ->
      context = id: 'main_context'
      context2 = id: 'ctx_2'
      @context.pushContext @message, context
        .then () =>
          @context.pushContext @message, context2
            .then () =>
              context3 = id: 'ctx_2', name: 'new'
              @context.updateContext @message, context3
                .then () =>
                  @context.getFullContextStack @message
                    .should.eventually.deep.equal [context, context3]


    it 'should pop undefined from empty stack', ->
      @context.popContext(@message)
        .then (cntx) ->
          should.not.exist(cntx)


    it 'should pop context from stack', ->
      context = id: 'main_context'
      @context.pushContext @message, context
        .then () =>
          @context.popContext(@message)
            .then (cntx) =>
              @context.getContext(@message)
                .then (data) ->
                  should.not.exist(data)
              return cntx
        .catch (err) ->
          throw err
        .should.eventually.equal context


    it 'should pop contexts until target context, including it', () ->
      context = {id: 'not_lunch'}
      @context.pushContext(@message, context)
      @context.pushContext(@message, {id: 'lunch'})
      @context.pushContext(@message, {id: 'top'})

      @context.popContextsUntilContext(@message, 'lunch')
        .then () =>
          @context.getFullContextStack @message
          .should.eventually.deep.equal [context]


    it 'should return whole context stack', ->
      context1 = id: 'main_context'
      context2 = id: 'second_context'

      @context.pushContext @message, context1
        .then () =>
          @context.pushContext @message, context2
            .then () =>
              @context.getFullContextStack @message
              .should.eventually.deep.equal [context1, context2]


    describe 'Middleware', ->

      it 'should register receive middleware', ->
        middleware_call = sinon.spy()
        next_call = sinon.spy()
        error_next_call = sinon.spy()
        @controller.middleware.receive.use = (clbck) =>
          middleware_call()
          clbck({}, @message, next_call)
            .then () ->
              next_call.should.have.been.called
          clbck({}, null, error_next_call)
            .catch () ->
              error_next_call.should.have.been.called

        @context = new Context(@controller, {silent: true})
        middleware_call.should.have.been.called


      it 'should add context to message', ->
        context = id: 'main_context'
        next_call = sinon.spy()
        @context = new Context(@controller)
        @controller.middleware.receive.use = (clbck) =>
          clbck({}, @message, next_call)
            .then () =>
              next_call.should.have.been.called
              @message.should.have.property 'context', context
              @message.context_stack.should.have.length 1
              @message.context_stack.should.be.deep.equal [context]

        @context.pushContext @message, context
          .then () =>
            @context.registerMiddleware()


    describe 'Hear middleware', ->

      it 'should return false if message does not have expected context', () ->
        filter = @context.expectedContexts(['lunch'])
        @context.pushContext(@message, {id: 'not_lunch'})
        @context.addContextToMessage(@message)
          .then () =>
            filter(['hey hey'], @message).should.be.false


      it 'should return true if message has expected context', () ->
        filter = @context.expectedContexts(['lunch'])
        @context.pushContext(@message, {id: 'lunch'})
        @context.addContextToMessage(@message)
          .then () =>
            filter(['hey hey'], @message).should.be.true


      it 'should return false if message has unexpected top context', () ->
        filter = @context.expectedTopContext('lunch')
        @context.pushContext(@message, {id: 'lunch'})
        @context.pushContext(@message, {id: 'not_lunch'})
        @context.addContextToMessage(@message)
          .then () =>
            filter(['hey hey'], @message).should.be.false


      it 'should return true if top message context and target context match', () ->
        filter = @context.expectedTopContext('lunch')
        @context.pushContext(@message, {id: 'not_lunch'})
        @context.pushContext(@message, {id: 'lunch'})
        @context.addContextToMessage(@message)
          .then () =>
            filter(['hey hey'], @message).should.be.true


    describe 'Clean up', ->

      before () ->
        @clock = sinon.useFakeTimers()

      after () ->
        @clock.restore()


      it 'should remove context when ttl expired', () ->
        @context = new Context(@controller, {scope: 'channels', ttl: 1000})
        @context.pushContext(@message, {id: 'lunch'})
        @clock.tick(999)
        @context.getFullContextStack @message
          .then (context) =>
            context.should.have.length 1
            @clock.tick(2)
            @context.getFullContextStack @message
              .then (context) ->
                context.should.have.length 0
          .catch (err) ->
            throw err
