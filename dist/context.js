var Context,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

Context = (function() {
  var delay, localStorage, log_error, silent, storage;

  silent = false;

  storage = {};

  log_error = function(error) {
    if (!silent) {
      return console.error(error);
    }
  };

  delay = function(delay, cb) {
    return setTimeout(cb, delay);
  };

  localStorage = {
    channels: {
      get: function(id, clbck) {
        return clbck(null, storage.channels[id]);
      },
      save: function(obj, clbck) {
        if (!obj.id) {
          return clbck('Id is not set');
        }
        storage.channels[obj.id] = obj;
        return clbck(null);
      },
      all: function(clbck) {
        var channel, id;
        return clbck(null, (function() {
          var ref, results;
          ref = storage.channels;
          results = [];
          for (id in ref) {
            channel = ref[id];
            results.push(channel);
          }
          return results;
        })());
      }
    },
    teams: {
      get: function(id, clbck) {
        return clbck(null, storage.teams[id]);
      },
      save: function(obj, clbck) {
        if (!obj.id) {
          return clbck('Id is not set');
        }
        storage.teams[obj.id] = obj;
        return clbck(null);
      },
      all: function(clbck) {
        var id, team;
        return clbck(null, (function() {
          var ref, results;
          ref = storage.teams;
          results = [];
          for (id in ref) {
            team = ref[id];
            results.push(team);
          }
          return results;
        })());
      }
    },
    users: {
      get: function(id, clbck) {
        return clbck(null, storage.users[id]);
      },
      save: function(obj, clbck) {
        if (!obj.id) {
          return clbck('Id is not set');
        }
        storage.users[obj.id] = obj;
        return clbck(null);
      },
      all: function(clbck) {
        var id, user;
        return clbck(null, (function() {
          var ref, results;
          ref = storage.users;
          results = [];
          for (id in ref) {
            user = ref[id];
            results.push(user);
          }
          return results;
        })());
      }
    }
  };

  function Context(controller, options) {
    var ref, ref1, ref2, ref3;
    this.controller = controller;
    if (options == null) {
      options = {};
    }
    this.scope = (ref = options.scope) != null ? ref : 'channels', silent = (ref1 = options.silent) != null ? ref1 : false, this.ttl = (ref2 = options.ttl) != null ? ref2 : 600000, this.storageType = (ref3 = options.storageType) != null ? ref3 : 'memory';
    this.scopeName = this.scope.substr(0, this.scope.length - 1);
    if (this.storageType === 'controller') {
      this.storage = this.controller.storage;
    } else {
      storage = {
        channels: {},
        teams: {},
        users: {}
      };
      this.storage = localStorage;
    }
    this.registerMiddleware();
  }

  Context.prototype.registerMiddleware = function() {
    return this.controller.middleware.receive.use((function(_this) {
      return function(bot, message, next) {
        return _this.addContextToMessage(message).then(function() {
          next();
        })["catch"](function(err) {
          log_error(err);
          return next();
        });
      };
    })(this));
  };

  Context.prototype.addContextToMessage = function(message) {
    return this.getFullContextStack(message).then(function(stack) {
      message.context_stack = stack;
      if (stack.length) {
        return message.context = stack[stack.length - 1];
      }
    });
  };

  Context.prototype.pushContext = function(message, context) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        if (!context) {
          return reject(new Error('No context given'));
        }
        if (!message[_this.scopeName]) {
          return reject(new Error("Msg property " + _this.scopeName + " is not set"));
        }
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          if (err) {
            return reject(err);
          }
          data = data || {
            id: message[_this.scopeName]
          };
          data.context_stack = data.context_stack || [];
          if (typeof context === 'string') {
            context = {
              id: context
            };
          }
          context.timestamp = +(new Date());
          data.context_stack.push(context);
          return _this.storage[_this.scope].save(data, function(err) {
            if (err) {
              return reject(err);
            }
            delay(_this.ttl, function() {
              return _this.popContext(message);
            });
            return resolve(context);
          });
        });
      };
    })(this));
  };

  Context.prototype.popContext = function(message) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          var poped_context;
          if (err) {
            return reject(err);
          }
          if (data && data.context_stack && data.context_stack.length) {
            poped_context = data.context_stack.pop();
            return _this.storage[_this.scope].save(data, function(err) {
              if (err) {
                return reject(err);
              }
              return resolve(poped_context);
            });
          } else {
            return resolve();
          }
        });
      };
    })(this));
  };

  Context.prototype.getContext = function(message) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          if (err) {
            return reject(err);
          }
          if (data && data.context_stack && data.context_stack.length) {
            return resolve(data.context_stack[data.context_stack.length - 1]);
          } else {
            return resolve();
          }
        });
      };
    })(this));
  };

  Context.prototype.updateContext = function(message, ctx) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          if (err) {
            return reject(err);
          }
          if (data && data.context_stack && data.context_stack.length) {
            return resolve(data.context_stack[data.context_stack.length - 1]);
          } else {
            return reject('Context stack is empty');
          }
        });
      };
    })(this));
  };

  Context.prototype.popContextsUntilContext = function(message, contextId) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        if (!message[_this.scopeName]) {
          return reject("Msg property " + _this.scopeName + " not valid");
        }
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          var ctx, i, j, len, stack;
          if (err) {
            return reject(err);
          }
          if (!data || !data.context_stack) {
            resolve();
            return;
          }
          stack = data.context_stack;
          i = 0;
          for (i = j = 0, len = stack.length; j < len; i = ++j) {
            ctx = stack[i];
            if (ctx.id === contextId) {
              break;
            }
          }
          console.log('--->', i);
          stack.splice(i);
          data.context_stack = stack;
          return _this.storage[_this.scope].save(data, function(err) {
            if (err) {
              return reject(err);
            }
            delay(_this.ttl, function() {
              return _this.popContext(message);
            });
            return resolve();
          });
        });
      };
    })(this));
  };

  Context.prototype.getFullContextStack = function(message) {
    return new Promise((function(_this) {
      return function(resolve, reject) {
        if (!message[_this.scopeName]) {
          return reject("Msg property " + _this.scopeName + " not valid");
        }
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          if (err) {
            return reject(err);
          }
          return resolve(data && data.context_stack || []);
        });
      };
    })(this));
  };

  Context.prototype.expectedContexts = function(targetContexts) {
    if (targetContexts == null) {
      targetContexts = [];
    }
    return function(patterns, message) {
      var context, j, k, len, len1, pattern, ref, ref1;
      ref = message.context_stack;
      for (j = 0, len = ref.length; j < len; j++) {
        context = ref[j];
        if (ref1 = context.id, indexOf.call(targetContexts, ref1) >= 0) {
          for (k = 0, len1 = patterns.length; k < len1; k++) {
            pattern = patterns[k];
            if (typeof pattern === 'string') {
              pattern = new RegExp(pattern);
            }
            if (pattern.test(message.text)) {
              return true;
            }
          }
        }
      }
      return false;
    };
  };

  Context.prototype.expectedTopContext = function(targetContext) {
    return function(patterns, message) {
      var j, len, pattern;
      if (message.context && message.context.id === targetContext) {
        for (j = 0, len = patterns.length; j < len; j++) {
          pattern = patterns[j];
          if (typeof pattern === 'string') {
            pattern = new RegExp(pattern);
          }
          if (pattern.test(message.text)) {
            return true;
          }
        }
        return false;
      }
      return false;
    };
  };

  Context.prototype.killAllContexts = function(message) {
    if (message == null) {
      message = null;
    }
    return new Promise((function(_this) {
      return function(resolve, reject) {
        return _this.storage[_this.scope].get(message[_this.scopeName], function(err, data) {
          if (err) {
            return reject(err);
          }
          if (data && data.context_stack) {
            data.context_stack = [];
            return _this.storage[_this.scope].save(data, function(err) {
              if (err) {
                return reject(err);
              }
              return resolve();
            });
          } else {
            return resolve();
          }
        });
      };
    })(this));
  };

  return Context;

})();

module.exports = Context;
