var gulp = require('gulp')
var coffee = require('gulp-coffee')
var gutil = require('gulp-util')
var mocha = require('gulp-spawn-mocha')

const build_dir = './dist'
const DEBUG = process.env.NODE_ENV === 'debug'


gulp.task('build_js', () => {
  return gulp.src('src/**/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest(build_dir))
})

gulp.task('test', () => {
  return gulp.src(['test/*.test.coffee'], {read: false})
    .pipe(mocha({
      debugBrk: DEBUG,
      compilers: 'coffee:coffee-script/register'
    }))
})

gulp.task('watch', () => {
  gulp.watch(['./src/**/*'], ['build_js']);
});

gulp.task('default', ['build_js']);
